<%-- 
    Document   : listado
    Created on : 01-05-2021, 04:37:43
    Author     : Ricardo
--%>


<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.palabras.entity.Registropalabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Registropalabras> lista = (List<Registropalabras>) request.getAttribute("lista");
    Iterator<Registropalabras> itpalabras = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="form" action="PalabrasController" method="POST">
            <table border="1" style="margin: 0 auto;" >
                <td colspan="2"><h1>Lista de Palabras</h1></td>
                    <tr>
                        <td><h2>Fecha</h2></td>
                        <td><h2>Palabra</h2></td>
                    </tr>
                  <tbody>
                    <%while (itpalabras.hasNext()) {
                            Registropalabras rp = itpalabras.next();%>
                    <tr>
                        <td><%=rp.getFecha()%></td>
                        <td><%=rp.getPalabra()%></td>

                    </tr>
                    <%}%>                
                </tbody>
                <tr>
                    <td colspan="2" >
                        <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
                    </td>

                </tr>

            </table>  
        </form>
    </body>
</html>
