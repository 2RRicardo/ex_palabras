<%-- 
    Document   : edita
    Created on : 08-05-2021, 06:15:53
    Author     : Ricardo
--%>

<%@page import="com.mycompany.palabras.entity.Registropalabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Registropalabras lista = (Registropalabras) request.getAttribute("lista");

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>editar palabra</h1>
        <form name="form" action="PalabrasController" method="POST">
            <fieldset>
                <legend>Ingrese Palabra :</legend>
                <table>
                    <tr>  
                        <td><label><input type="text" name="palabra" value="<%= lista.getPalabra()%>"></label>/td>
                        <td><label><input type="text" name="palabra" value="<%= lista.getFecha()%>"></label>/td>
                    </tr>
                </table>
            </fieldset>  

            <fieldset>
                <legend>Significado:</legend>
                <table>
                    <tr>
                        <td><label><textarea rows="4" cols="50" name="comment"  ></textarea></label>    </td>
                    </tr>
                </table>
            </fieldset>
            <button type="submit" name="accion" value="verlista" class="btn btn-success">Listar Palabras</button>
        </form>
    </body>
</html>
