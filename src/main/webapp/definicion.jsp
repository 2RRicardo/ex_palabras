<%-- 
    Document   : definicion
    Created on : May 8, 2021, 12:17:28 AM
    Author     : Andrea
--%>

<%@page import="com.mycompany.palabras.entity.Registropalabras"%>
<!DOCTYPE html>
<%
    Registropalabras definicion = (Registropalabras) request.getAttribute("definicion");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definición</title>
    </head>
    <body>
        
        <form  name="form" action="PalabrasController" method="POST">
            <h1><%= definicion.getPalabra().toString() %></h1>
            <h2><%= definicion.getFecha().toString() %></h2> 
            <h2>Fuente: Oxford Dictionaries</h2>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
         </form>
    </body>
</html>
