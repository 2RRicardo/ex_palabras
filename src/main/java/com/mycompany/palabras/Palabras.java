/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.palabras;

import cl.dto.PalabraDTO;
import com.mycompany.palabras.dao.RegistropalabrasJpaController;
import com.mycompany.palabras.dao.exceptions.NonexistentEntityException;
import com.mycompany.palabras.entity.Registropalabras;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.New;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Ricardo
 */
@Path("palabras")
public class Palabras {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPalabras() {

        RegistropalabrasJpaController dao = new RegistropalabrasJpaController();
        List<Registropalabras> listadao = dao.findRegistropalabrasEntities();

        return Response.ok(200).entity(listadao).build();

    }

    /*@GET
    @Path("/idbuscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar) {

        Client cliente = ClientBuilder.newClient();
        WebTarget myResource = cliente.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idbuscar);
        PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("app_id", "93a0813e").header("app_key", "b43be0edc79dda59ba969392cb60af28").get(PalabraDTO.class);

        Registropalabras pal = new Registropalabras();

        pal.setPalabra(palabradto.getWord());
        Date fecha = new Date();
        pal.setFecha(fecha.toString());
        return Response.ok(200).entity(pal).build();
    }*/

    @GET
    @Path("/idbuscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultaPalabra(@PathParam("idbuscar") String idbuscar) {

        RegistropalabrasJpaController dao = new RegistropalabrasJpaController();
        Registropalabras palabra = dao.findRegistropalabras(idbuscar);

        return Response.ok(200).entity(palabra).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response Add(Registropalabras regpalabras) {

        try {
            RegistropalabrasJpaController dao = new RegistropalabrasJpaController();
            dao.create(regpalabras);

            return Response.ok(200).entity(regpalabras).build();
        } catch (Exception ex) {
            Logger.getLogger(Palabras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(regpalabras).build();
    }

    @DELETE
    @Path("/iddelete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete) {
        try {
            RegistropalabrasJpaController dao = new RegistropalabrasJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Palabras.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Cliente Eliminado").build();
    }

    /*buscar significado con API externa*/
}
