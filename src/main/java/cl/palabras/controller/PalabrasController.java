/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.palabras.controller;

import com.mycompany.palabras.entity.Registropalabras;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Ricardo
 */
@WebServlet(name = "PalabrasController", urlPatterns = {"/PalabrasController"})
public class PalabrasController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PalabrasController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PalabrasController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion = request.getParameter("accion");

        if (accion.equals("ingreso")) {
            request.getRequestDispatcher("ingreso.jsp").forward(request, response);

        }
        if (accion.equals("guardarpalabra")) {
            Registropalabras regpalabras = new Registropalabras();
            regpalabras.setPalabra(request.getParameter("palabra"));
            regpalabras.setFecha(request.getParameter("fecha"));
            Client cliente = ClientBuilder.newClient();
            WebTarget myResourcel = cliente.target("http://LAPTOP-0KJKPIRR:8080/Palabras-1.0-SNAPSHOT/api/palabras");
            Registropalabras regpalabras1 = myResourcel.request(MediaType.APPLICATION_JSON).post(Entity.json(regpalabras), Registropalabras.class);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        if (accion.equals("verlista")) {
            Client cliente = ClientBuilder.newClient();
            WebTarget myResourcel = cliente.target("http://LAPTOP-0KJKPIRR:8080/Palabras-1.0-SNAPSHOT/api/palabras");
            List<Registropalabras> lista = (List<Registropalabras>) myResourcel.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Registropalabras>>() {
            });
            request.setAttribute("lista", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);

        }
       /* System.out.println("valor accion"+accion);*/
        if (accion.equals("eliminar")) {
            String idElimminar = request.getParameter("seleccion");
            Client cliente1 = ClientBuilder.newClient();
            WebTarget myResourcel = cliente1.target("http://LAPTOP-0KJKPIRR:8080/Palabras-1.0-SNAPSHOT/api/palabras"+idElimminar);
            myResourcel.request(MediaType.APPLICATION_JSON).delete();
            request.getRequestDispatcher("index.jsp").forward(request, response);
          }
        
        if (accion.equals("volver")) {
            
            request.getRequestDispatcher("index.jsp").forward(request, response);
        
        }
        
        if (accion.equals("definicion")) {
            
            request.getRequestDispatcher("definicion.jsp").forward(request, response);
        
        }
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
