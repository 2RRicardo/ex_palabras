/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dto;

import java.util.ArrayList;

/**
 *
 * @author Ricardo
 */
public class PalabraDTO {
    private String id;
  Metadata MetadataObject;
  ArrayList<Object> results = new ArrayList<Object>();
  private String word;


 // Getter Methods 

  public String getId() {
    return id;
  }

  public Metadata getMetadata() {
    return MetadataObject;
  }

  public String getWord() {
    return word;
  }

 // Setter Methods 

  public void setId( String id ) {
    this.id = id;
  }

  public void setMetadata( Metadata metadataObject ) {
    this.MetadataObject = metadataObject;
  }

  public void setWord( String word ) {
    this.word = word;
  }
}

